﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace test.ViewModels
{
    public class BMIData
    {
        [Display(Name="體重")]
        [Required(ErrorMessage ="必填欄位")]
        [Range(30,150,ErrorMessage ="請填入30~150的數值")]
        public float? W { get; set; }
        [Display(Name = "身高")]
        [Required(ErrorMessage = "必填欄位")]
        [Range(50, 200, ErrorMessage = "請填入50~200的數值")]
        public float? H { get; set; }
        public float? Bmi { get; set; }
        public String Lev { get; set; }
    }
}