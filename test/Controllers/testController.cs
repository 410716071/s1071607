﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace test.Controllers
{
    public class testController : Controller
    {
        // GET: test
        public ActionResult Index()
        {
            ViewData["aa"] = "abcd";
            ViewData["b"] = "efgh";
            ViewBag.a = 1;
            return View();
        }
        public ActionResult Html()
        {
            return View();
        }
        public ActionResult HtmlHelper()
        {
            return View();
        }

        public ActionResult Razor()
        {
            return View();
        }
    }
}