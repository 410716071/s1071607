﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using test.ViewModels;

namespace test.Controllers
{
    public class BMIController : Controller
    {
        public ActionResult Index()
        {
            return View(new BMIData());
        }



        [HttpPost]
        public ActionResult Index(BMIData data)
        {
            

            if (ModelState.IsValid)
            {
                var m_h = data.H / 100;
                var re = data.W / (m_h * m_h);
                var le = "";
                if (re < 18.5) {
                    le  = "體重過輕";
                }
                else if (re < 24)
                {
                    le = "正常範圍";
                }
                else if (re < 27)
                {
                    le = "過重";
                }
                else if (re < 30)
                {
                    le = "輕度肥胖";
                }
                else if (re < 35)
                {
                    le = "中度肥胖";
                }
                else if (re > 35)
                {
                    le = "重度肥胖";
                }
                data.Bmi = re;
                data.Lev = le;
          

            }
            return View(data);
        }
    }
}